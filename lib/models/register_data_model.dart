class RegisterDataModel {
  final String reportUsername, reportEmail, reportPassword;

  RegisterDataModel(
      this.reportUsername,
      this.reportEmail,
      this.reportPassword);
}